
/*
 * Display message in div with id 'messages'.
 * Class may be 'success', 'info', 'warning' or 'danger'. 'danger' is default value.
 */
function errMsg(msg /*String*/, cls/*String*/) {
    var msgArea = $('#messages');
    if (msgArea) {
        msgArea.html('');
        
        if (!msg) { // clear if empty msg
            return;
        }

        var alert = $('<div>').addClass('alert alert-dismissible').attr({role : 'alert'});
        alert.addClass(cls ? 'alert-' + cls : 'alert-danger');
        alert.append($('<button>').addClass('close').attr({type : 'button', 'aria-label' : 'Close',  'data-dismiss' : 'alert'})
            .append($('<span>').attr({'aria-hidden' : 'true'}).html('&times;')));
        alert.append($('<span>').text(msg));
        msgArea.append(alert);
    }
}