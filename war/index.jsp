<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page isELIgnored ="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.Locale" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:choose>
    <c:when test="${!(empty param.locale)}">
        <%
          Locale locale = Locale.forLanguageTag(request.getParameter("locale"));
          if (locale != null) {
            session.setAttribute("locale", locale);
            session.setAttribute("language", locale.getLanguage());
          }
        %>
    </c:when>
    <c:when test="${!(empty sessionScope.language)}">
       <%-- do nothing actually. language selector already in the session --%>
    </c:when>
    <c:otherwise>
        <c:set var="locale" value="${pageContext.request.locale}" scope="session"/>
        <%
            String[] supportedLang = {"ru", "uk"};
            Locale locale = (Locale) session.getAttribute("locale");
            String currentLanguage = locale.getLanguage();
            boolean setDefault = true;
            for (int i = 0; i < supportedLang.length; i++) {
                if (supportedLang[i].equalsIgnoreCase(currentLanguage)) {
                    setDefault = false;
                    break;
                }
            }
            if (setDefault) {
                session.setAttribute("language", "en");
                session.setAttribute("locale", Locale.US);
            } else {
                session.setAttribute("language", currentLanguage);
            }
        %>
    </c:otherwise>
</c:choose>
<fmt:setLocale value="${sessionScope.language}" />
<fmt:setBundle basename="org.cycling.wheeler.server.locale.WheelerUiMessagesServer" />
<jsp:useBean id="calc" class="org.cycling.wheeler.server.CalcBean" scope="session"/>
<jsp:setProperty name="calc" property="*"/>

<html lang="${sessionScope.language}">
<head>
  <%@include file="/include/head.jsp" %>
  <link type="text/css" rel="stylesheet" href="wheeler.css">
  <link rel="alternate" href="android-app://org.cycling.wheeler/http/spoke-calc.appspot.com/index" />

  <meta name="description" content="<fmt:message key='PageDesc' />">
  <meta name="keywords" content="<fmt:message key='PageKeyword' />">
  <meta name='yandex-verification' content='4ed18869a97dd9aa'>
  <meta name='wmail-verification' content='35cbe546d72541a5'>

  <%@include file="/include/analytics.jsp" %>

  <title><fmt:message key="PageTitle" /></title>

</head>
<body>

  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-xs-12 col-md-8">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#wheeler-top-menu">
              <span class="sr-only"><fmt:message key="ToggleNavigation"/></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><fmt:message key="Wheeler"/></a>
          </div>

          <div class="collapse navbar-collapse" id="wheeler-top-menu">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/help"><fmt:message key="Help"/></a></li>
             <%
                UserService userService = UserServiceFactory.getUserService();
                User user = userService.getCurrentUser();
                    if (user != null) {
                         pageContext.setAttribute("user", user);
             %>
                <li><a href="<%= userService.createLogoutURL(request.getRequestURI()) %>"><fmt:message key="LogOut"/></a></li>
             <%
                } else {
             %>
                <li><a href="<%= userService.createLoginURL(request.getRequestURI()) %>"><fmt:message key="LogIn"/></a></li>
             <%
                }
             %>
            </ul>

		</div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <%@include file="/include/noscript.jsp" %>

    <%-- save wheelset modal window --%>
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="shareWinTitle" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="shareWinTitle"><fmt:message key="TitleSaveWheelset" /></h4>
          </div>
          <div class="modal-body">
            <label for="wheelsetName" class="control-label"><fmt:message key="enter_name" /></label>
            <input type="text" class="form-control input-sm save-modal-field" id="wheelsetName" value="${calc.name}" placeholder="<fmt:message key='wheelset_name' />" >
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" id="closeSaveBtn" type="button"><fmt:message key="ButtonClose" /></button>
            <c:choose>
            <c:when test="${empty calc.key}">
              <button class="btn btn-default hidden" id="newWhsetBtn" type="button"><fmt:message key="ButtonNew" /></button>
            </c:when>
            <c:otherwise>
              <button class="btn btn-default" id="newWhsetBtn" type="button"><fmt:message key="ButtonNew" /></button>
            </c:otherwise>
            </c:choose>
            <button class="btn btn-primary" id="saveWhsetBtn" type="button"><fmt:message key="ButtonSave" /></button>
          </div>
        </div>
      </div>
    </div>
    <%-- end of modal window --%>

    <div class="row" style="margin-bottom: 0.5em;">
      <div class="col-md-2"></div>
      <div class="col-xs-12 col-md-8">
        <ul class="nav nav-pills nav-justified">
          <li role="presentation" class="active"><a href="/index"><fmt:message key="ButtonCalculate" /></a></li>
          <li role="presentation"><a href="/mywheels"><fmt:message key="ButtonWheelsets" /></a></li>
        </ul>
      </div>
      <div class="col-md-2"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-xs-12 col-md-8">

        <div class="panel panel-default">
          <div class="panel-heading"><b><fmt:message key="MainTitle"/></b></div>
          <div class="panel-body">

            <div class="row">
              <div class="col-xs-12">
                <form class="form-horizontal" role="form" name="CalcForm" method="post" action="index">

                  <%-- get email address modal window --%>
                  <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareWinTitle" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="shareWinTitle"><fmt:message key="TitleSendByEmail" /></h4>
                        </div>
                        <div class="modal-body">
                          <label for="emailTo" class="control-label"><fmt:message key="To" /></label>
                          <input type="email" class="form-control input-sm share-modal-field" id="emailTo" name="emailTo" value="" placeholder="<fmt:message key='PlaceholderEmailTo' />" >
                          <label for="emailSubject" class="control-label"><fmt:message key="Subject" /></label>
                          <input type="text" class="form-control input-sm share-modal-field" id="emailSubject" name="emailSubject" value="" >
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-default" id="closeEmailBtn" type="button"><fmt:message key="ButtonClose" /></button>
                          <button class="btn btn-primary" id="sendEmailBtn" type="submit" formaction="/send"><fmt:message key="ButtonSend" /></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <%-- end of modal window --%>

                  <div class="form-group section-header" style="margin-top: 1em;">
                    <div class="col-xs-12"><fmt:message key="Rim" /></div>
                    <div class="hr-padding"><hr></div>
                  </div>

                  <div class="form-group">
                    <label for="effectiveRimDiameter" class="col-sm-5 control-label"><fmt:message key="EffectiveDiameter" /></label>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" class="form-control input-sm" id="effectiveRimDiameter" name="effectiveRimDiameter" value="${calc.effectiveRimDiameter}" autofocus required>
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="offset" class="col-sm-5 control-label"><fmt:message key="Offset" /></label>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" class="form-control input-sm" id="offset" name="offset" step="0.01" value="${calc.offset}" >
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group section-header">
                    <div class="col-xs-12"><fmt:message key="Hub" /></div>
                    <div class="hr-padding"><hr></div>
                  </div>

                  <div class="form-group">
                    <label for="hubLength" class="col-sm-5 control-label"><fmt:message key="TotalLength" /></label>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" class="form-control input-sm" id="hubLength" name="hubLength" value="${calc.hubLength}" required>
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="leftFlangeDiameter" class="col-sm-5 control-label"><fmt:message key="FlangeDiameter" /></label>
                    <label for="rightFlangeDiameter" class="sr-only"><fmt:message key="FlangeDiameter" /></label>
                    <div class="col-sm-3">
                      <div class="input-group extra-space">
                        <input type="number" step="0.1" class="form-control input-sm" id="leftFlangeDiameter" name="leftFlangeDiameter" value="${calc.leftFlangeDiameter}" required placeholder="<fmt:message key="SideLeft" />" >
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" step="0.1" class="form-control input-sm" id="rightFlangeDiameter" name="rightFlangeDiameter" value="${calc.rightFlangeDiameter}" required placeholder="<fmt:message key="SideRight"/>">
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="leftFlangeDistance" class="col-sm-5 control-label"><fmt:message key="FlangeDistance" /></label>
                    <label for="rightFlangeDistance" class="sr-only"><fmt:message key="FlangeDistance" /></label>
                    <div class="col-sm-3">
                      <div class="input-group extra-space">
                        <input type="number" step="0.1" class="form-control input-sm" id="leftFlangeDistance" name="leftFlangeDistance" value="${calc.leftFlangeDistance}" required placeholder="<fmt:message key="SideLeft" />">
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" step="0.1" class="form-control input-sm" id="rightFlangeDistance" name="rightFlangeDistance" value="${calc.rightFlangeDistance}" required placeholder="<fmt:message key="SideRight" />">
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="spokeHole" class="col-sm-5 control-label"><fmt:message key="DiamSpokeHole" /></label>
                    <div class="col-sm-3">
                      <div class="input-group">
                        <input type="number" step="0.1" class="form-control input-sm" id="spokeHole" name="spokeHole" value="${calc.spokeHole}" required>
                        <span class="input-group-addon"><fmt:message key="UomMm" /></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group section-header">
                    <div class="col-xs-12"><fmt:message key="Spokes" /></div>
                    <div class="hr-padding"><hr></div>
                  </div>

                  <c:set var="numberSpokesDef" value="${calc.numberSpokes}" scope="request"/>
                  <c:if test="${empty numberSpokesDef}">
                    <c:set var="numberSpokesDef" value="32" scope="request"/>
                  </c:if>

                  <div class="form-group">
                    <label for="numberSpokes" class="col-sm-5 control-label"><fmt:message key="NumberOfSpokes" /></label>
                    <div class="col-sm-3">
                      <select name="numberSpokes" id="numberSpokes" class="form-control input-sm">
                        <option value="20"<c:if test="${20==numberSpokesDef}"> selected</c:if>>20</option>
                        <option value="24"<c:if test="${24==numberSpokesDef}"> selected</c:if>>24</option>
                        <option value="28"<c:if test="${28==numberSpokesDef}"> selected</c:if>>28</option>
                        <option value="32"<c:if test="${32==numberSpokesDef}"> selected</c:if>>32</option>
                        <option value="36"<c:if test="${36==numberSpokesDef}"> selected</c:if>>36</option>
                        <option value="40"<c:if test="${40==numberSpokesDef}"> selected</c:if>>40</option>
                        <option value="48"<c:if test="${48==numberSpokesDef}"> selected</c:if>>48</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="leftCrossing" class="col-sm-5 control-label"><fmt:message key="NumberOfIntersections" /></label>
                    <label for="rightFlangeDistance" class="sr-only"><fmt:message key="NumberOfIntersections" /></label>
                    <div class="col-sm-3">
                      <c:set var="leftCrossingDef" value="${calc.leftCrossing}" scope="request" />
                      <c:if test="${empty leftCrossingDef}">
                        <c:set var="leftCrossingDef" value="3" scope="request" />
                      </c:if>
                      <select name="leftCrossing" id="leftCrossing" class="form-control input-sm extra-space">
                        <option value="0"<c:if test="${0==leftCrossingDef}"> selected</c:if>>0</option>
                        <option value="1"<c:if test="${1==leftCrossingDef}"> selected</c:if>>1</option>
                        <option value="2"<c:if test="${2==leftCrossingDef}"> selected</c:if>>2</option>
                        <option value="3"<c:if test="${3==leftCrossingDef}"> selected</c:if>>3</option>
                        <option value="4"<c:if test="${4==leftCrossingDef}"> selected</c:if>>4</option>
                        <option value="5"<c:if test="${5==leftCrossingDef}"> selected</c:if>>5</option>
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <c:set var="rightCrossingDef" value="${calc.rightCrossing}" scope="request" />
                      <c:if test="${empty rightCrossingDef}">
                        <c:set var="rightCrossingDef" value="3" scope="request" />
                      </c:if>
                      <select name="rightCrossing" id="rightCrossing" class="form-control input-sm">
                        <option value="0"<c:if test="${0==rightCrossingDef}"> selected</c:if>>0</option>
                        <option value="1"<c:if test="${1==rightCrossingDef}"> selected</c:if>>1</option>
                        <option value="2"<c:if test="${2==rightCrossingDef}"> selected</c:if>>2</option>
                        <option value="3"<c:if test="${3==rightCrossingDef}"> selected</c:if>>3</option>
                        <option value="4"<c:if test="${4==rightCrossingDef}"> selected</c:if>>4</option>
                        <option value="5"<c:if test="${5==rightCrossingDef}"> selected</c:if>>5</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group section-header" style="margin-bottom: 0;">
                    <div class="col-xs-12"><fmt:message key="Results"/></div>
                    <div class="hr-padding"><hr></div>
                  </div>

                  <c:set var="leftLen" value="${calc.leftSpokeLength}" scope="request" />
                  <c:set var="rightLen" value="${calc.rightSpokeLength}" scope="request" />
                  <c:set var="leftLenRounded" value="${calc.leftSpokeLengthRounded}" scope="request" />
                  <c:set var="rightLenRounded" value="${calc.rightSpokeLengthRounded}" scope="request" />

                  <input type="hidden" name="leftSpokeLength" value="${leftLen}" />
                  <input type="hidden" name="rightSpokeLength" value="${rightLen}" />
                  <input type="hidden" name="leftSpokeLengthRounded" value="${leftLenRounded}" />
                  <input type="hidden" name="rightSpokeLengthRounded" value="${rightLenRounded}" />

                  <div class="form-group">
                    <label class="col-xs-5 control-label"><fmt:message key="SpokeLengthPrecise"/></label>
                    <div class="col-xs-3">
                      <p class="form-control-static">${leftLen}</p>
                    </div>
                    <div class="col-xs-3">
                      <p class="form-control-static">${rightLen}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-5 control-label"><fmt:message key="SpokeLengthRounded"/></label>
                    <div class="col-xs-3">
                      <p class="form-control-static">${leftLenRounded}</p>
                    </div>
                    <div class="col-xs-3">
                      <p class="form-control-static">${rightLenRounded}</p>
                    </div>
                  </div>

                  <div id="messages">
                  </div>

                  <div class="center-block" style="margin-top: 2em;text-align: center">
                    <button type="submit" class="btn btn-primary"><fmt:message key="ButtonCalculate" /></button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#shareModal"<c:if test="${empty user}"> disabled="disabled"</c:if>><fmt:message key="ButtonSend"/></button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#saveModal"<c:if test="${empty user}"> disabled="disabled"</c:if>><fmt:message key="ButtonSave"/></button>
                    <button type="button" class="btn btn-default" id="do-clear"<c:if test="${empty calc.key}"> disabled="disabled"</c:if>><fmt:message key="ButtonClear"/></button>
                  </div>

                </form>
              </div>
            </div>

          </div> <%-- end of panel body --%>
        </div>

      </div>
      <div class="col-md-2"></div>
    </div>
  </div>

  <%@include file="/include/bottom-links.jsp" %>

  <%@include file="/include/js.jsp" %>

  <script>
    $("#sendEmailBtn").click(function() {
        $('#shareModal').modal('hide');
    });
    $("#closeEmailBtn").click(function() {
        $('#shareModal').modal('hide');
        $('.share-modal-field').val('');
    });
    $('#saveWhsetBtn').on('click', function() {
        var wheelsetName = $('#wheelsetName').val();
        var params = $(document.CalcForm).serialize();
        params += '&wheelsetName=' + wheelsetName;
        $.post('/whs', params, function(data) {
            if (data && data._ERROR_MESSAGE_) {
                errMsg(data._ERROR_MESSAGE_);
                return;
            }
        }, 'json');
        $('#saveModal').modal('hide');
    });
    $('#newWhsetBtn').on('click', function() {
            var wheelsetName = $('#wheelsetName').val();
            var params = $(document.CalcForm).serialize();
            params += '&wheelsetName=' + wheelsetName;
            params += '&new=Y'
            $.post('/whs', params, function(data) {
                if (data && data._ERROR_MESSAGE_) {
                    errMsg(data._ERROR_MESSAGE_);
                    return;
                }
            }, 'json');
            $('#saveModal').modal('hide');
    });
    $('#closeSaveBtn').on('click', function() {
        $('#saveModal').modal('hide');
        $('.save-modal-field').val('');
    });
    $('#do-clear').on('click', function() {
        window.location = '/clear';
    });
  </script>

</body>
</html>
