<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page isELIgnored ="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.Locale" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:choose>
    <c:when test="${!(empty param.locale)}">
        <c:set var="language" value="${param.locale}" scope="session" />
        <%
          String locale = request.getParameter("locale");
          if (locale != null) {
              session.setAttribute("locale", new Locale(locale));
          }
        %>
    </c:when>
    <c:when test="${!(empty sessionScope.language)}">
       <%-- do nothing actually. language selector already in the session --%>
    </c:when>
    <c:otherwise>
        <c:set var="locale" value="${pageContext.request.locale}" scope="session"/>
        <%
            String[] supportedLang = {"ru", "uk"};
            Locale locale = (Locale) session.getAttribute("locale");
            String currentLanguage = locale.getLanguage();
            boolean setDefault = true;
            for (int i = 0; i < supportedLang.length; i++) {
                if (supportedLang[i].equalsIgnoreCase(currentLanguage)) {
                    setDefault = false;
                    break;
                }
            }
            if (setDefault) {
                session.setAttribute("language", "en");
                session.setAttribute("locale", Locale.US);
            } else {
                session.setAttribute("language", currentLanguage);
            }
        %>
    </c:otherwise>
</c:choose>
<fmt:setLocale value="${sessionScope.language}" />
<fmt:setBundle basename="org.cycling.wheeler.server.locale.WheelerUiMessagesServer" />
<jsp:useBean id="calc" class="org.cycling.wheeler.server.CalcBean" scope="request"/>
<jsp:setProperty name="calc" property="*"/>

<html lang="${sessionScope.language}">
<head>
  <%@include file="/include/head.jsp" %>
  <link type="text/css" rel="stylesheet" href="wheeler.css">
  <link rel="alternate" href="android-app://org.cycling.wheeler/http/spoke-calc.appspot.com/help" />

  <meta name="description" content="<fmt:message key='PageDescHelp' />">
  <meta name="keywords" content="<fmt:message key='PageKeywordHelp' />">

  <%@include file="/include/analytics.jsp" %>

  <title><fmt:message key="PageTitleHelp" /></title>

</head>
<body>

  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-xs-12 col-md-8">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#wheeler-top-menu">
              <span class="sr-only"><fmt:message key="ToggleNavigation"/></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><fmt:message key="Wheeler"/></a>
          </div>

          <div class="collapse navbar-collapse" id="wheeler-top-menu">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/"><fmt:message key="MenuClose"/></a></li>
             <%
                UserService userService = UserServiceFactory.getUserService();
                User user = userService.getCurrentUser();
                    if (user != null) {
                         pageContext.setAttribute("user", user);
             %>
                <li><a href="<%= userService.createLogoutURL(request.getRequestURI()) %>"><fmt:message key="LogOut"/></a></li>
             <%
                } else {
             %>
                <li><a href="<%= userService.createLoginURL(request.getRequestURI()) %>"><fmt:message key="LogIn"/></a></li>
             <%
                }
             %>
            </ul>

        </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <%@include file="/include/noscript.jsp" %>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-xs-12 col-md-8">

        <div class="panel panel-default">
          <div class="panel-heading"><b><fmt:message key="HelpTitle"/></b></div>
          <div class="panel-body">

            <c:choose>
                <c:when test="${language == 'ru'}"><%@include file="/help/ru/measurements.jsp" %></c:when>
                <c:when test="${language == 'uk'}"><%@include file="/help/uk/measurements.jsp" %></c:when>
                <c:otherwise><%@include file="/help/en/measurements.jsp" %></c:otherwise>
            </c:choose>

          </div> <%-- end of panel body --%>
        </div>

      </div>
      <div class="col-md-2"></div>
    </div>

    <%@include file="/include/bottom-links.jsp" %>

  </div>

  <%@include file="/include/js.jsp" %>

</body>
</html>
