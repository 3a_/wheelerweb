<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored ="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
--%>
<c:choose>
    <c:when test="${!(empty param.locale)}">
        <c:set var="language" value="${param.locale}"/>
    </c:when>
    <c:otherwise>
        <c:set var="language" value="${pageContext.request.locale}" />
        <%
            String[] supportedLang = {"ru", "uk"};
            String currentLanguage = (String) request.getAttribute("language");
            boolean setDefault = true;
            for (int i = 0; i < supportedLang.length; i++) {
                if (supportedLang[i].equals(currentLanguage)) {
                    setDefault = false;
                    break;
                }
            }
            if (setDefault) {
                request.setAttribute("language", "en");
            }
        %>
    </c:otherwise>
</c:choose>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="org.cycling.wheeler.server.locale.WheelerUiMessagesServer" />
<html lang="${language}">
    <head>

        <title><fmt:message key="Help" /></title>
        
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="/help/help.css">

        <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/images/wheeler.ico" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

		<div class="container">
            <c:choose>
                <c:when test="${language == 'ru'}"><jsp:include page="/help/ru/measurements.jsp" /></c:when>
                <c:when test="${language == 'uk'}"><%@include file="/help/uk/measurements.jsp" %></c:when>
                <c:otherwise><%@include file="/help/en/measurements.jsp" %></c:otherwise>
            </c:choose>
		</div>

        <script src="//code.jquery.com/jquery.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    </body>
</html>
