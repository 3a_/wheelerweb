<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-xs-12 topic_header">
        <h3>Измерения</h3>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4><strong>Обод</strong></h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p> Измерения обода необходимы для нахождения его эффективного диаметра.
            Имеется в виду, что вам необходимо найти расстояние
            между концами полностью натянутых спиц (концы должны быть на
            уровне торца шляпки нипеля). Для этого поместите нипель в
            обод и померяйте насколько глубоко находится шляпка
            относительно внешнего края обода. Умножте этот размер на два
            и отнимите полученное значение от внешнего диаметра обода.</p>

        <p>Помните, что в процессе сборки колеса спица
            вытягивается, а обод, в зависимости от его прочности, может
            сжиматься. Поэтому расчётную длину спиц всегда нужно
            округлять в меньшую сторону до ближайшего доступного
            размера. Но не стоит допускать разницу между расчётной и
            реальной длиной более 2мм.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <img src=/images/help/measure-rim-mdpi-ru.png class="img-responsive" />
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4><strong>Втулка</strong></h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p>Диаметр фланца меряется по центрам отверстий под
            спицы.Линейные размеры-как обозначено на рисунке.</p>
    </div>
</div>

<div class="row visible-xs">
    <div class="col-xs-3"></div>
    <div class="col-xs-6">
        <img src=/images/help/measure-flange-mdpi-ru.png class="img-responsive" />
    </div>
    <div class="col-xs-3"></div>
</div>

<div class="row visible-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub-mdpi-ru.png class="img-responsive" />
    </div>
</div>

<div class="row hidden-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub_flange-mdpi-ru.png class="img-responsive" />
    </div>
</div>

