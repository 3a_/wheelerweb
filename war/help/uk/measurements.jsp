<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-xs-12 topic_header">
        <h3>Вимірювання</h3>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4><strong>Обод</strong></h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p>Вимірювання ободу необхідні для знаходження його ефективного
            діаметру. Мається на увазі, що вам необхідно знайти відстань
            між кінцями повністю натягнутих спиць (кінці повинні бути на
            рівні торця ніпеля). Для цього помістіть ніпель в обід і
            поміряйте, наскільки глибоко він знаходиться щодо зовнішнього
            краю обода. Помножте цей розмір на два і відніміть отримане
            значення від зовнішнього діаметра обода.</p>

        <p>Пам'ятайте, що в процесі складання колеса спиця
            витягується, а обід, залежно від його міцності, може
            стискатися. Тому розрахункову довжину спиць завжди
            потрібно округляти в меншу сторону до найближчого доступного
            розміру. Але не варто допускати різницю між розрахунковою і
            реальною довжиною більше 2мм.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <img src=/images/help/measure-rim-mdpi-uk.png class="img-responsive" />
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4>
            <strong>Втулка</strong>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p>Діаметр фланця міряється за центрами отворів під спиці. Лінійні розміри - як позначено на малюнку.</p>
    </div>
</div>

<div class="row visible-xs">
    <div class="col-xs-3"></div>
    <div class="col-xs-6">
        <img src=/images/help/measure-flange-mdpi-uk.png class="img-responsive" />
    </div>
    <div class="col-xs-3"></div>
</div>

<div class="row visible-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub-mdpi-uk.png class="img-responsive" />
    </div>
</div>

<div class="row hidden-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub_flange-mdpi-uk.png class="img-responsive" />
    </div>
</div>

