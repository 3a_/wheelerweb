<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="row">
    <div class="col-xs-12 topic_header">
        <h3>Measurements</h3>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4>
            <strong>Rim</strong>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p>In order to measure a rim you have to find its effective
            diameter. The effective rim diameter is the diameter to
            which the ends of the fully engaged spokes will extend
            (flush with the end of the spoke nipple). Drop a nipple into
            a rim hole and find how far its head lies below the outer
            edge of the rim. This value should be doubled and subtracted
            from the outer rim diameter. Count the holes to find opposed
            points on the rim.</p>

        <p>Note that a spoke under full tension can stretch up and
            the rim, depending on its strength, can also shrink in
            diameter. Therefore, the computed spoke length should always
            be rounded down to the nearest available size. But do not
            allow the difference between the calculated and actual
            length more than 2mm.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <img src=/images/help/measure-rim-mdpi-en.png class="img-responsive" />
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h4><strong>Hub</strong></h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p>The hub flange diameter is the distance from center to center of the spoke holes.
        And see the figure to understand linear dimensions.</p>
    </div>
</div>

<div class="row visible-xs">
    <div class="col-xs-3"></div>
    <div class="col-xs-6">
        <img src=/images/help/measure-flange-mdpi-en.png class="img-responsive" />
    </div>
    <div class="col-xs-3"></div>
</div>

<div class="row visible-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub-mdpi-en.png class="img-responsive" />
    </div>
</div>

<div class="row hidden-xs">
    <div class="col-xs-12">
        <img src=/images/help/measure-hub_flange-mdpi-en.png class="img-responsive" />
    </div>
</div>
