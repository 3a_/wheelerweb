<!DOCTYPE html>
<%@ page pageEncoding="UTF-8" %>
<%@ page isELIgnored ="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.Locale" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:choose>
    <c:when test="${!(empty param.locale)}">
        <c:set var="language" value="${param.locale}" scope="session" />
        <%
          String locale = request.getParameter("locale");
          if (locale != null) {
              session.setAttribute("locale", new Locale(locale));
          }
        %>
    </c:when>
    <c:when test="${!(empty sessionScope.language)}">
       <%-- do nothing actually. language selector already in the session --%>
    </c:when>
    <c:otherwise>
        <c:set var="locale" value="${pageContext.request.locale}" scope="session"/>
        <%
            String[] supportedLang = {"ru", "uk"};
            Locale locale = (Locale) session.getAttribute("locale");
            String currentLanguage = locale.getLanguage();
            boolean setDefault = true;
            for (int i = 0; i < supportedLang.length; i++) {
                if (supportedLang[i].equalsIgnoreCase(currentLanguage)) {
                    setDefault = false;
                    break;
                }
            }
            if (setDefault) {
                session.setAttribute("language", "en");
                session.setAttribute("locale", Locale.US);
            } else {
                session.setAttribute("language", currentLanguage);
            }
        %>
    </c:otherwise>
</c:choose>
<fmt:setLocale value="${sessionScope.language}" />
<fmt:setBundle basename="org.cycling.wheeler.server.locale.WheelerUiMessagesServer" />
<jsp:useBean id="calc" class="org.cycling.wheeler.server.CalcBean" scope="request"/>
<jsp:setProperty name="calc" property="*"/>

<html lang="${sessionScope.language}">
<head>
  <%@include file="/include/head.jsp" %>
  <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
  <link type="text/css" rel="stylesheet" href="wheeler.css">

  <%-- TODO: unique set of description, keyword and title for this page --%>
  <meta name="description" content="<fmt:message key='PageDescWheelsets' />">
  <meta name="keywords" content="<fmt:message key='PageKeywordWheelsets' />">

  <%@include file="/include/analytics.jsp" %>

  <title><fmt:message key="PageTitleWheelsets" /></title>

</head>
<body>
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-xs-12 col-md-8">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#wheeler-top-menu">
              <span class="sr-only"><fmt:message key="ToggleNavigation"/></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><fmt:message key="Wheeler"/></a>
          </div>

          <div class="collapse navbar-collapse" id="wheeler-top-menu">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/help"><fmt:message key="Help"/></a></li>
             <%
                UserService userService = UserServiceFactory.getUserService();
                User user = userService.getCurrentUser();
                    if (user != null) {
                         pageContext.setAttribute("user", user);
             %>
                <li><a href="<%= userService.createLogoutURL(request.getRequestURI()) %>"><fmt:message key="LogOut"/></a></li>
             <%
                } else {
             %>
                <li><a href="<%= userService.createLoginURL(request.getRequestURI()) %>"><fmt:message key="LogIn"/></a></li>
             <%
                }
             %>
            </ul>

        </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <%@include file="/include/noscript.jsp" %>

    <div class="row" style="margin-bottom: 0.5em;">
      <div class="col-md-2"></div>
      <div class="col-xs-12 col-md-8">
        <div id="messages"></div>
        <ul class="nav nav-pills nav-justified">
          <li role="presentation"><a href="/index"><fmt:message key="ButtonCalculate" /></a></li>
          <li role="presentation" class="active"><a href="/mywheels"><fmt:message key="ButtonWheelsets" /></a></li>
        </ul>
      </div>
      <div class="col-md-2"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-xs-12 col-md-8">
        
        <div class="panel panel-default">
          <div class="panel-heading"><b><fmt:message key="StoredWheelsets" /></b></div>
          <div class="panel-body">
            <jsp:include page="/include/wheelsets.jsp" />
          </div>
        </div>

      </div>
      <div class="col-md-2"></div>
    </div>
  </div>

  <%@include file="/include/bottom-links.jsp" %>
  <%@include file="/include/js.jsp" %>
  <script src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
  <script src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
  <script>
  $(document).ready(function(){
      var table = $('#wheelsetTable').DataTable({
          language: {
              url: '<fmt:message key="DataTablesTranslation"/>'
          }
      });
      table.column('1:visible').order('asc').draw();
      $('a.removebtn').on('click', function(e) {
          return $.ajax({
              url: ('/whs?key=' + $(e.target).data("key")),
              type: 'DELETE',
              dataType: 'json',
              statusCode: {
                  400: function() {
                      errMsg('<fmt:message key="ErrNoValidIdentifier" />');
                  },
                  401: function() {
                      errMsg('<fmt:message key="ErrDeleteWheelsetAuth" />');
                  },
                  404: function() {
                      errMsg('<fmt:message key="ErrWheelsetNotFound" />');
                  }
              }
            }).done(function() {
                table.row($('#row_' + $(e.target).data("key"))).remove().draw(false);
            }).fail(function(resp, status, msg) {
                console.log(status + ': ' + msg);
            });
      });
      $('#wheelsetTable tbody .wclickable').on('click', function(e) {
          console.log(this);
          $.ajax({
              url: '/whs',
              data: {key : $(this).closest('tr').attr('id')},
              statusCode: {
                  400: function() {
                      errMsg('<fmt:message key="ErrNoValidIdentifier" />');
                  },
                  401: function() {
                      errMsg('<fmt:message key="ErrGetWheelsetAuth" />');
                  },
                  404: function() {
                      errMsg('<fmt:message key="ErrWheelsetNotFound" />');
                  }
              }
          }).done(function() {
              window.location = '/';
          }).fail(function(resp, status, msg) {
              console.log(status + ': ' + msg);
          });
      });
  });
  </script>

</body>