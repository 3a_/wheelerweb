<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page isELIgnored ="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="org.cycling.wheeler.shared.DatabaseWorker" %>
<%@ page import="com.google.appengine.api.users.*" %>
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="org.cycling.wheeler.server.locale.WheelerUiMessagesServer" />
<%
UserService userService = UserServiceFactory.getUserService();
User currentUser = userService.getCurrentUser();
pageContext.setAttribute("user", currentUser);
pageContext.setAttribute("wheelsets", DatabaseWorker.findWheelsetsAll(currentUser));
%>
<c:if test="${empty wheelsets}">
  <c:choose>
    <c:when test="${empty user}">
      <fmt:message key="NoUserWheelsetsEmtpyMsg" />
    </c:when>
    <c:otherwise>
      <fmt:message key="NoWheelsetsEmptyMsg" />
    </c:otherwise>
  </c:choose>
</c:if>

<c:if test="${!empty wheelsets}">
<table id="wheelsetTable" class="table table-hover" data-page-length="25">
    <thead>
        <tr>
            <th><fmt:message key="ColumnName" /></th>
            <th width="25%"><fmt:message key="ColumnCreated" /></th>
            <th width="5%" data-sortable="false"></th>
        </tr>
    </thead>
    <tbody>
      <c:forEach items="${wheelsets}" var="item">
      <tr id="row_${item.key}">
        <td class="wclickable"><c:out value="${item.name}" /></td>
        <td class="wclickable"><fmt:formatDate value="${item.dateCreated}" type="BOTH" dateStyle="MEDIUM" timeStyle="SHORT" /></td>
        <td align="right"><a href="javascript: void(0)" class="removebtn"><span data-key="${item.key}" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
      </tr>
      </c:forEach>
    </tbody>
</table>
</c:if>