<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
    <noscript>
      <div style="width: 39em; margin-left: auto; margin-right: auto; color: red; border: 1px solid red; padding: 4px; font-family: sans-serif; text-align: center;">
        <fmt:message key="NoScriptCaution" />
      </div>
    </noscript>

