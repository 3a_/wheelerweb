/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
*/
package org.cycling.wheeler.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Named;

import org.apache.commons.validator.GenericValidator;
import org.cycling.wheeler.model.Wheel;
import org.cycling.wheeler.model.Wheelset;
import org.cycling.wheeler.shared.Calc;
import org.cycling.wheeler.shared.DatabaseWorker;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.response.BadRequestException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.users.User;

@Api(
        name = "wheelsets",
        version = "v1",
        scopes = {Constants.EMAIL_SCOPE},
        clientIds = {Constants.WEB_CLIENT_ID, Constants.ANDROID_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID},
        audiences = {Constants.ANDROID_AUDIENCE}
)
public class Wheelsets {
    private static final String LOGGER = Wheelsets.class.getName();
    private static final String OAUTH_ERR_MSG = "Method <%1$s> can be called only by an authorized user";

    public List<Wheelset> list(User user) throws OAuthRequestException {
        // check if user is logged in
        if (user == null) {
            throw new OAuthRequestException(String.format(OAUTH_ERR_MSG, "listWheelsets"));
        }

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        Key groupRoot = DatabaseWorker.updateUserRef(user, ds);

        List<Wheelset> resultList = new ArrayList<Wheelset>();

        Query userWheelsets = new Query(Constants.Wheelset.KIND).setAncestor(groupRoot);
        List<Entity> wheelsets = ds.prepare(userWheelsets).asList(FetchOptions.Builder.withDefaults());
        if (wheelsets.size() > 0) {
            for (Entity wheelset : wheelsets) {
                Wheelset w = new Wheelset();
                String name = (String) wheelset.getProperty(Constants.Wheelset.P_NAME);
                // name is mandatory property.
                // filter out the case when no wheelsets but user login exists and query
                // returns single record where only data from UserLogin
                if (!GenericValidator.isBlankOrNull(name)) {
                    w.setName(name);
                    Text desc = (Text) wheelset.getProperty(Constants.Wheelset.P_DESCR);
                    if (desc != null) {
                        w.setDescription(desc.getValue());
                    }
                    w.setDateCreated((Date) wheelset.getProperty(Constants.Wheelset.P_CREATED));
                    w.setDateChanged((Date) wheelset.getProperty(Constants.Wheelset.P_CHANGED));
                    w.setKey(KeyFactory.keyToString(wheelset.getKey()));
                    resultList.add(w);
                }
            }
        }

        return resultList;
    }

    public Wheelset get(@Named("id") String id, @Named("itemsOnly") Boolean itemsOnly, User user) throws OAuthRequestException, BadRequestException {
        // check if user is logged in
        if (user == null) {
            throw new OAuthRequestException(String.format(OAUTH_ERR_MSG, "getWheelset"));
        }
        if (GenericValidator.isBlankOrNull(id)) {
            throw new BadRequestException("In order to retrieve a wheelset you have to provide valid identifier");
        }

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        DatabaseWorker.updateUserRef(user, ds);

        try {
            Key wheelsetKey = KeyFactory.stringToKey(id);
            Entity wheelset = ds.get(wheelsetKey);

            // ensure the user is owner of the wheelset to delete
            Entity userLogin = ds.get(wheelset.getParent());
            Email owner = (Email) userLogin.getProperty(Constants.UserLogin.P_USER_LOGIN_ID);
            if (user.getEmail().equalsIgnoreCase(owner.getEmail())) {
                Wheelset result = new Wheelset(id, itemsOnly == null || !itemsOnly ? wheelset.getProperties() : null);
                List<Entity> wheels = ds.prepare(new Query(wheelsetKey)).asList(FetchOptions.Builder.withDefaults());
                if (wheels != null && wheels.size() > 0) {
                    List<Wheel> items = new ArrayList<Wheel>(wheels.size());
                    for (Entity w : wheels) {
                        if (Constants.Wheel.KIND.equals(w.getKind())) {
                            Wheel wheel = new Wheel(KeyFactory.keyToString(w.getKey()), w.getProperties());
                            items.add(wheel);
                        }
                    }
                    result.setWheels(items);
                }

                return result;
            }

        } catch (Exception e) {
            Logger.getLogger(LOGGER).log(Level.SEVERE, String.format("Unable to return wheelset [%1$s] due to error", id), e);
            return null;
        }

        return null;
    }

    public Map<String, Object> update(Wheelset ws, User user) throws OAuthRequestException, BadRequestException {
        // check if user is logged in
        if (user == null) {
            throw new OAuthRequestException(String.format(OAUTH_ERR_MSG, "updateWheelset"));
        }
        if (ws == null) {
            throw new BadRequestException("Please, provide wheelset data for update operation.");
        }

        Map<String, Object> results = new HashMap<String, Object>();

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        Transaction tx = ds.beginTransaction();

        String id = null;

        try {
            Key groupRoot = DatabaseWorker.updateUserRef(user, ds);

            Date now = new Date();

            Entity wheelset = null;

            id = ws.getKey();
            if (!GenericValidator.isBlankOrNull(id)) {
                Key key = KeyFactory.stringToKey(id);
                try {
                    wheelset = ds.get(key);
                } catch (EntityNotFoundException e) {
                    wheelset = new Entity(Constants.Wheelset.KIND, groupRoot);
                }
            } else {
                wheelset = new Entity(Constants.Wheelset.KIND, groupRoot);
            }

            wheelset.setProperty(Constants.Wheelset.P_NAME, ws.getName()); // name is mandatory
            String wsDesc = ws.getDescription();
            if (wsDesc != null) {
                wheelset.setProperty(Constants.Wheelset.P_DESCR, new Text(wsDesc));
            }

            wheelset.setProperty(Constants.Wheelset.P_CREATED, ws.getDateCreated() == null ? now : ws.getDateCreated());
            wheelset.setProperty(Constants.Wheelset.P_CHANGED, ws.getDateChanged() == null ? now : ws.getDateChanged());
            wheelset.setProperty(Constants.Wheelset.P_GID, ws.getGid());

            Key wheelsetKey = ds.put(wheelset);
            results.put("wheelsetKey", KeyFactory.keyToString(wheelsetKey));

            List<Wheel> wheels = ws.getWheels();
            // right now we supports only wheel per wheelset
            if (wheels != null && wheels.size() > 0) {
                for (Wheel w : wheels) {
                    String wheelId = w.getKey();
                    Entity wheelEntity =
                            GenericValidator.isBlankOrNull(wheelId) ? new Entity(Constants.Wheel.KIND, wheelsetKey) : new Entity(KeyFactory.stringToKey(wheelId));

                    DatabaseWorker.setWheelProps(wheelEntity, w);

                    ds.put(wheelEntity);
                }
            }

            tx.commit();
        } catch (RuntimeException e) {
            results.put(Constants.ERROR_MESSAGE, e.getLocalizedMessage());
            Logger.getLogger(LOGGER).log(Level.SEVERE, String.format("Unable to update wheelset [%1$s] due to unexpected runtime error", id), e);
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }

        return results;
    }

    @ApiMethod(name="add", httpMethod="post")
    public Map<String, Object> add(@Named("wheelsetId") String wheelsetId, Wheel w, User user) throws OAuthRequestException, BadRequestException {
        // check if user is logged in
        if (user == null) {
            throw new OAuthRequestException(String.format(OAUTH_ERR_MSG, "updateWheelset"));
        }
        if (wheelsetId == null || w == null) {
            throw new BadRequestException("Please, provide correct data adding new wheel.");
        }

        Map<String, Object> results = new HashMap<String, Object>();

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        Transaction tx = ds.beginTransaction();

        try {
            DatabaseWorker.updateUserRef(user, ds);

            Key wheelsetKey = KeyFactory.stringToKey(wheelsetId);

            Entity wheel = new Entity(Constants.Wheel.KIND, wheelsetKey);
            DatabaseWorker.setWheelProps(wheel, w);

            Key wheelKey = ds.put(wheel);
            if (wheelKey != null) {
                results.put("wheelKey", KeyFactory.keyToString(wheelKey));
            }

            tx.commit();

        } catch (RuntimeException e) {
            results.put(Constants.ERROR_MESSAGE, e.getLocalizedMessage());
            Logger.getLogger(LOGGER).log(Level.SEVERE, String.format("Unable to add a wheel to the wheelset [%1$s] due to unexpected runtime error", wheelsetId), e);
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }

        return results;
    }

    public Map<String, Object> delete(@Named("id") String id, User user) throws OAuthRequestException, BadRequestException {
        // check if user is logged in
        if (user == null) {
            throw new OAuthRequestException(String.format(OAUTH_ERR_MSG, "deleteWheelset"));
        }
        if (GenericValidator.isBlankOrNull(id)) {
            throw new BadRequestException("Please, provide a valid identifier for delete operation");
        }

        Map<String, Object> results = new HashMap<String, Object>();

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        DatabaseWorker.updateUserRef(user, ds);

        try {

            Key wheelsetKey = KeyFactory.stringToKey(id);
            Entity wheelset = ds.get(wheelsetKey);

            // ensure the user is owner of the wheelset to delete
            Entity userLogin = ds.get(wheelset.getParent());
            Email owner = (Email) userLogin.getProperty(Constants.UserLogin.P_USER_LOGIN_ID);
            if (user.getEmail().equalsIgnoreCase(owner.getEmail())) {
                Query q = new Query(wheelsetKey).setKeysOnly();
                List<Entity> childWheels = ds.prepare(q).asList(FetchOptions.Builder.withDefaults());
                if (childWheels != null && childWheels.size() > 0) {
                    List<Key> keys = new ArrayList<Key>(childWheels.size());
                    for (Entity e : childWheels) {
                        keys.add(e.getKey());
                    }
                    ds.delete(keys);
                }
            } else {
                results.put(Constants.ERROR_MESSAGE, "Only owner can delete the wheelset");
                return results;
            }

        } catch(Exception e) {
            results.put(Constants.ERROR_MESSAGE, e.getLocalizedMessage());
            Logger.getLogger(LOGGER).log(Level.SEVERE, String.format("Unable to delete wheelset [%1$s] due to error",  id), e);
        }

        return results;
    }

    @ApiMethod(name="calculate", httpMethod="get")
    public Map<String, Double> calculate(
            @Named("erd") Double effectiveRimDiameter,
            @Named("offset") Double rimOffset,
            @Named("lFlangeDiam") Double leftFlangeDiameter,
            @Named("rFlangeDiam") Double rightFlangeDiameter,
            @Named("lFlangeDist") Double leftFlangeDistance,
            @Named("rFlangeDist") Double rightFlangeDistance,
            @Named("old") Double hubLength,
            @Named("hole") Double spokeHole,
            @Named("spokes") Integer numSpokes,
            @Named("lcrossing") Integer leftNumCrossing,
            @Named("rcrossing") Integer rightNumCrossing
            ) {
        Map<String, Double> results = new HashMap<String, Double>();

        Calc calc = new Calc();
        calc.setEffectiveRimDiameter(effectiveRimDiameter);
        calc.setRimOffset(rimOffset);
        calc.setLeftFlangeDiameter(leftFlangeDiameter);
        calc.setRightFlangeDiameter(rightFlangeDiameter);
        calc.setLeftFlangeDistance(leftFlangeDistance);
        calc.setRightFlangeDistance(rightFlangeDistance);
        calc.setLeftNumCrossing(leftNumCrossing);
        calc.setRightNumCrossing(rightNumCrossing);
        calc.setHubLength(hubLength);
        calc.setSpokeHole(spokeHole);
        calc.setNumSpokes(numSpokes);

        Double leftLength = calc.getLeftLen();
        if (leftLength != null) {
            results.put("left", leftLength);
            results.put("leftRounded", calc.getLeftLenRounded(leftLength));
        }
        Double rightLength = calc.getRightLen();
        if (rightLength != null) {
            results.put("right", rightLength);
            results.put("rightRounded", calc.getRightLenRounded(rightLength));
        }

        return results;
    }
}
