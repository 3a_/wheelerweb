/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
*/
package org.cycling.wheeler.api;

public class Constants {
//    public static final String WEB_CLIENT_ID = "1077494222847-oul27p0aqtcagd9etknuggpf8n9lj7ai.apps.googleusercontent.com";
//    public static final String ANDROID_CLIENT_ID = "1077494222847-frtaiu3a28nib3be7m803c17cc7hml08.apps.googleusercontent.com";
    public static final String WEB_CLIENT_ID = "198242553532.apps.googleusercontent.com";
    public static final String ANDROID_CLIENT_ID = "198242553532-pkkoocavja2lhu9lggp9c49s9onu1dkj.apps.googleusercontent.com";
    public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

    public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";

    public static final String ERROR_MESSAGE = "_ERROR_MESSAGE";

    public static class UserLogin {
        public static final String KIND = "UserLogin";

        public static final String P_USER_LOGIN_ID = "userLoginId";
        public static final String P_LAST_LOGGED_IN = "lastLoggedIn";
        public static final String P_USED_COUNT = "usedCount";
    }

    public static class Wheelset {
        public static final String KIND = "Wheelset";

        public static final String P_NAME = "name";
        public static final String P_DESCR = "description";
        public static final String P_CREATED = "dateCreated";
        public static final String P_CHANGED = "dateChanged";
        public static final String P_GID = "gid";
    }

    public static class Wheel {
        public static final String KIND = "Wheel";

        public static final String P_NAME = "name";
        public static final String P_RIM = "rim";
        public static final String P_ERD = "effectiveRimDiameter";
        public static final String P_OFFSET = "rimOffset";
        public static final String P_HUB = "hub";
        public static final String P_HUB_LENGTH = "hubLength";
        public static final String P_L_FLANGE_DIAM = "leftFlangeDiameter";
        public static final String P_R_FLANGE_DIAM = "rightFlangeDiameter";
        public static final String P_L_FLANGE_DIST = "leftFlangeDistance";
        public static final String P_R_FLANGE_DIST = "rightFlangeDistance";
        public static final String P_SPOKE_HOLE = "spokeHole";
        public static final String P_SPOKES = "spokes";
        public static final String P_SPOKE_NUM = "spokeNum";
        public static final String P_L_INTERSECTIONS = "leftIntersections";
        public static final String P_R_INTERSECTIONS = "rightIntersections";
        public static final String P_L_LENGTH = "leftLength";
        public static final String P_R_LENGTH = "rightLength";
    }
}
