/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.server;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.validator.GenericValidator;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class ShareServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(ShareServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	Locale locale = CommonUtils.getLocale(req);
    	ResourceBundle labels = ResourceBundle.getBundle("org.cycling.wheeler.server.locale.WheelerUiMessagesServer", locale);

    	String effectiveRimDiameter = req.getParameter("effectiveRimDiameter");
        String offset = req.getParameter("offset");
        String offsetFormated = GenericValidator.isBlankOrNull(offset) ? "" : offset + " " + labels.getString("UomMM");
        String totalLength = req.getParameter("hubLength") ;
        String leftFlangeDiameter = req.getParameter("leftFlangeDiameter");
        String rightFlangeDiameter = req.getParameter("rightFlangeDiameter");
        String leftFlangeDistance = req.getParameter("leftFlangeDistance");
        String rightFlangeDistance = req.getParameter("rightFlangeDistance");
        String diamSpokeHole = req.getParameter("spokeHole");
        String numberOfSpokes  = req.getParameter("numberSpokes");
        String leftCrossing = req.getParameter("leftCrossing");
        String rightCrossing = req.getParameter("rightCrossing");
        String leftSpokeLength = req.getParameter("leftSpokeLength");
        String leftSpokeLengthFormated = GenericValidator.isBlankOrNull(leftSpokeLength) ? "" : leftSpokeLength + " " + labels.getString("UomMM");
        String rightSpokeLength = req.getParameter("rightSpokeLength");
        String rightSpokeLengthFormated = GenericValidator.isBlankOrNull(rightSpokeLength) ? "" : rightSpokeLength + " " + labels.getString("UomMM");
        String leftSpokeLengthRounded = req.getParameter("leftSpokeLengthRounded");
        String leftSpokeLengthRoundedFormated = GenericValidator.isBlankOrNull(leftSpokeLengthRounded) ? "" : leftSpokeLengthRounded + " " + labels.getString("UomMM");
        String rightSpokeLengthRounded = req.getParameter("rightSpokeLengthRounded");
        String rightSpokeLengthRoundedFormated = GenericValidator.isBlankOrNull(rightSpokeLengthRounded) ? "" : rightSpokeLengthRounded + " " + labels.getString("UomMM");
        String emailTo = req.getParameter("emailTo") ;
        String emailSubject = req.getParameter("emailSubject");
        String emailSubjectFormatted = GenericValidator.isBlankOrNull(emailSubject) ? "" : emailSubject ;

        // prepare message body
        String calcText = labels.getString("ShareInitialValues") + "\n"
        		+ labels.getString("ShareRimErd") + ": " + effectiveRimDiameter + " " + labels.getString("UomMM") + "\n"
        		+ labels.getString("ShareRimOffset") + ": " + offsetFormated + "\n"
        		+ labels.getString("ShareHubOld") + ": " + totalLength + " " + labels.getString("UomMM") + "\n"
        		+ labels.getString("ShareFlangeDiameter") + ": " + leftFlangeDiameter + " " + labels.getString("UomMM") + "/" + rightFlangeDiameter + " " + labels.getString("UomMM") + "\n"
        		+ labels.getString("ShareFlangeDistance") + ": " + leftFlangeDistance + " " + labels.getString("UomMM") + "/" + rightFlangeDistance + " " + labels.getString("UomMM") + "\n"
        		+ labels.getString("ShareSpokeHole") + ": " + diamSpokeHole + " " + labels.getString("UomMM") + "\n"
        		+ labels.getString("ShareNumSpokes") + ": " + numberOfSpokes + "\n"
        		+ labels.getString("ShareNumIntersections") + ": " + leftCrossing + "/" + rightCrossing + "\n"
        		+ "\n" + labels.getString("ShareResults") + "\n"
        		+ labels.getString("ShareLengthPrecise") + ": " + (!GenericValidator.isBlankOrNull(leftSpokeLengthFormated) || !GenericValidator.isBlankOrNull(rightSpokeLengthFormated) ? leftSpokeLengthFormated + "/" + rightSpokeLengthFormated + "\n" : "\n")
        		+ labels.getString("ShareLengthRounded") + ": " + (!GenericValidator.isBlankOrNull(leftSpokeLengthRoundedFormated) || !GenericValidator.isBlankOrNull(rightSpokeLengthRoundedFormated) ? leftSpokeLengthRoundedFormated + "/" + rightSpokeLengthRoundedFormated + "\n" : "\n");

        // send message
        Properties props = new Properties();
        Session sessionForMail = Session.getDefaultInstance(props, null);

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser(); 

        if (user != null) {
            String userMail = user.getEmail();
            log.info(String.format("Sending message to %1$s", userMail));

            try{
                Message msg = new MimeMessage(sessionForMail);
                msg.setFrom(new InternetAddress( userMail , null));
                msg.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(emailTo , null));
                msg.setSubject(emailSubjectFormatted);
                msg.setText(calcText);

                Transport.send(msg);

            } catch (AddressException e) {
                log.severe(e.getLocalizedMessage());
            } catch (MessagingException e) {
                log.severe(e.getLocalizedMessage());
            }
        }

        resp.sendRedirect("/index");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
