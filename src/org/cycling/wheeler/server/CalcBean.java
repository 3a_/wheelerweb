/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.server;

import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.commons.validator.GenericValidator;
import org.cycling.wheeler.shared.Calc;


/**
 * It's used to handle form with JSP pages.<br>
 * Every property should be named after corresponding form field.
 */
@SuppressWarnings("serial")
public class CalcBean implements Serializable {
    private String key;
    private String name;
    private boolean isDirty = false;
    private Calc calc;

    public CalcBean() {
        calc = new Calc();
    }

    public String getEffectiveRimDiameter() {
        return calc.getEffectiveRimDiameter() == null ? "" : calc.getEffectiveRimDiameter().toString();
    }
    public void setEffectiveRimDiameter(String effectiveRimDiameter) {
        if (!GenericValidator.isBlankOrNull(effectiveRimDiameter) && GenericValidator.isDouble(effectiveRimDiameter)) {
            isDirty = true;
            calc.setEffectiveRimDiameter(Double.parseDouble(effectiveRimDiameter));
        }
    }

    public String getOffset() {
        return calc.getRimOffset() == null ? "" : calc.getRimOffset().toString();
    }
    public void setOffset(String offset) {
        if (!GenericValidator.isBlankOrNull(offset) && GenericValidator.isDouble(offset)) {
            isDirty = true;
            Double val = Double.parseDouble(offset);
            calc.setRimOffset(val.compareTo(0.0) == 0 ? null : val);
        }
    }

    public String getLeftFlangeDiameter() {
        return calc.getLeftFlangeDiameter() == null ? "" : calc.getLeftFlangeDiameter().toString();
    }
    public void setLeftFlangeDiameter(String leftFlangeDiameter) {
        if (!GenericValidator.isBlankOrNull(leftFlangeDiameter) && GenericValidator.isDouble(leftFlangeDiameter)) {
            isDirty = true;
            calc.setLeftFlangeDiameter(Double.parseDouble(leftFlangeDiameter));
        }
    }

    public String getRightFlangeDiameter() {
        return calc.getRightFlangeDiameter() == null ? "" : calc.getRightFlangeDiameter().toString();
    }
    public void setRightFlangeDiameter(String rightFlangeDiameter) {
        if (!GenericValidator.isBlankOrNull(rightFlangeDiameter) && GenericValidator.isDouble(rightFlangeDiameter)) {
            isDirty = true;
            calc.setRightFlangeDiameter(Double.parseDouble(rightFlangeDiameter));
        }
    }

    public String getHubLength() {
        return calc.getHubLength() == null ? "" : calc.getHubLength().toString();
    }
    public void setHubLength(String hubLength) {
        if (!GenericValidator.isBlankOrNull(hubLength) && GenericValidator.isDouble(hubLength)) {
            isDirty = true;
            calc.setHubLength(Double.parseDouble(hubLength));
        }
    }

    public String getLeftFlangeDistance() {
        return calc.getLeftFlangeDistance() == null ? "" : calc.getLeftFlangeDistance().toString();
    }
    public void setLeftFlangeDistance(String leftFlangeDistance) {
        if (!GenericValidator.isBlankOrNull(leftFlangeDistance) && GenericValidator.isDouble(leftFlangeDistance)) {
            isDirty = true;
            calc.setLeftFlangeDistance(Double.parseDouble(leftFlangeDistance));
        }
    }

    public String getRightFlangeDistance() {
        return calc.getRightFlangeDistance() == null ? "" : calc.getRightFlangeDistance().toString();
    }
    public void setRightFlangeDistance(String rightFlangeDistance) {
        if (!GenericValidator.isBlankOrNull(rightFlangeDistance) && GenericValidator.isDouble(rightFlangeDistance)) {
            isDirty = true;
            calc.setRightFlangeDistance(Double.parseDouble(rightFlangeDistance));
        }
    }

    public String getSpokeHole() {
        return calc.getSpokeHole() == null ? "" : calc.getSpokeHole().toString();
    }
    public void setSpokeHole(String spokeHole) {
        if (!GenericValidator.isBlankOrNull(spokeHole) && GenericValidator.isDouble(spokeHole)) {
            isDirty = true;
            calc.setSpokeHole(Double.parseDouble(spokeHole));
        }
    }

    public String getNumberSpokes() {
        return calc.getNumSpokes() == null ? "" : calc.getNumSpokes().toString();
    }
    public void setNumberSpokes(String numberSpokes) {
        if (!GenericValidator.isBlankOrNull(numberSpokes) && GenericValidator.isInt(numberSpokes)) {
            isDirty = true;
            calc.setNumSpokes(Integer.parseInt(numberSpokes));
        }
    }

    public String getLeftCrossing() {
        return calc.getLeftNumCrossing() == null ? "" : calc.getLeftNumCrossing().toString();
    }
    public void setLeftCrossing(String leftCrossing) {
        if (!GenericValidator.isBlankOrNull(leftCrossing) && GenericValidator.isInt(leftCrossing)) {
            isDirty = true;
            calc.setLeftNumCrossing(Integer.parseInt(leftCrossing));
        }
    }

    public String getRightCrossing() {
        return calc.getRightNumCrossing() == null ? "" : calc.getRightNumCrossing().toString();
    }
    public void setRightCrossing(String rightCrossing) {
        if (!GenericValidator.isBlankOrNull(rightCrossing) && GenericValidator.isInt(rightCrossing)) {
            isDirty = true;
            calc.setRightNumCrossing(Integer.parseInt(rightCrossing));
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDirty() {
        return isDirty;
    }

    public void setDirty(boolean isDirty) {
        this.isDirty = isDirty;
    }

    public String getLeftSpokeLength() {
        Double len = calc.getLeftLen();
        if (len != null) {
            NumberFormat fmt = DecimalFormat.getInstance(/*TODO: locale?*/);
            fmt.setRoundingMode(RoundingMode.FLOOR);
            fmt.setMaximumFractionDigits(2);
            return fmt.format(len);
        }
        return "";
    } 

    public String getLeftSpokeLengthRounded() {
        Double len = calc.getLeftLenRounded(null);
        if (len != null) {
            NumberFormat fmt = DecimalFormat.getInstance(/*TODO: locale?*/);
            return fmt.format(len);
        }
        return "";
    }

    public String getRightSpokeLength() {
        Double len = calc.getRightLen();
        if (len != null) {
            NumberFormat fmt = DecimalFormat.getInstance(/*TODO: locale?*/);
            fmt.setRoundingMode(RoundingMode.FLOOR);
            fmt.setMaximumFractionDigits(2);
            return fmt.format(len);
        }
        return "";
    };

    public String getRightSpokeLengthRounded() {
        Double len = calc.getRightLenRounded(null);
        if (len != null) {
            NumberFormat fmt = DecimalFormat.getInstance(/*TODO: locale?*/);
            return fmt.format(len);
        }
        return "";
    }

    public Boolean isResultsReady() {
        Boolean ret = (getLeftSpokeLength() != null || getRightSpokeLength() != null);
        return ret;
    }

    /**
     * Sometimes we need Calc instance for direct access.
     */
    public Calc getCalcObject() {
        return calc;
    }
}
