/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.server;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.cycling.wheeler.model.Wheel;
import org.cycling.wheeler.model.Wheelset;
import org.cycling.wheeler.shared.Calc;
import org.cycling.wheeler.shared.DatabaseWorker;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;


@SuppressWarnings("serial")
public class WheelsetServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(WheelsetServlet.class.getName());

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if (user == null) {
            log.severe("Only authorized user can delete a wheelset");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        String id = req.getParameter("key");
        if (GenericValidator.isBlankOrNull(id)) {
            String msg = "Please, provide a valid identifier for delete operation";
            log.severe(msg);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
            return;
        }

        int code = DatabaseWorker.deleteWheelset(id, user);
        if (code != HttpServletResponse.SC_OK) {
            resp.sendError(code);
            return;
        }

        CommonUtils.writeJSONtoResponse("{}", resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if (user == null) {
            log.severe("Only authorized user can load a wheelset");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        String id = req.getParameter("key");
        if (GenericValidator.isBlankOrNull(id)) {
            String msg = "Please, provide a valid wheelset identifier";
            log.severe(msg);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
            return;
        }

        String key = id.substring(4);
        Wheelset wheelset = DatabaseWorker.getWheelset(key, false, user);
        if (wheelset == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        List<Wheel> wheels = wheelset.getWheels();
        //TODO: add support for multiple wheels
        Wheel wh = null;
        if (wheels != null && wheels.size() > 0) {
            wh = wheels.get(0);
        }

        CalcBean calcBean = new CalcBean();
        calcBean.setName(wheelset.getName());
        calcBean.setKey(key);

        if (wh != null) {
            Calc calc = calcBean.getCalcObject();
            calc.setKey(wh.getKey());

            calc.setEffectiveRimDiameter(wh.getEffectiveRimDiameter());
            calc.setRimOffset(wh.getRimOffset());
            calc.setHubLength(wh.getHubLength());
            calc.setLeftFlangeDiameter(wh.getLeftFlangeDiameter());
            calc.setRightFlangeDiameter(wh.getRightFlangeDiameter());
            calc.setLeftFlangeDistance(wh.getLeftFlangeDistance());
            calc.setRightFlangeDistance(wh.getRightFlangeDistance());
            calc.setSpokeHole(wh.getSpokeHole());
            calc.setNumSpokes(wh.getSpokeNum().intValue());
            calc.setLeftNumCrossing(wh.getLeftIntersections().intValue());
            calc.setRightNumCrossing(wh.getRightIntersections().intValue());
        }
        req.getSession().setAttribute("calc", calcBean);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        HttpSession session = req.getSession();
        Locale locale = CommonUtils.getLocale(req);
        ResourceBundle messages = ResourceBundle.getBundle("org.cycling.wheeler.server.locale.WheelerUiMessagesServer", locale);

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if (user == null) {
            log.severe("User does not exist. Log in first.");
            CommonUtils.writeJSONtoResponse("{}", resp);
            return;
        }

        CalcBean calc = null;
        boolean createNew = "Y".equalsIgnoreCase(req.getParameter("new"));
        if (createNew) {
            calc = new CalcBean();
            session.setAttribute("calc", calc);
        } else {
            calc = (CalcBean) session.getAttribute("calc");
        }

        String wheelsetName = req.getParameter("wheelsetName");
        if (GenericValidator.isBlankOrNull(wheelsetName)) {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("_ERROR_MESSAGE_", messages.getString("ErrWheelsetNameRequired"));
            Gson gson = new Gson();
            CommonUtils.writeJSONtoResponse(gson.toJson(result), resp);
            return;
        }

        calc.setEffectiveRimDiameter(req.getParameter("effectiveRimDiameter"));
        calc.setOffset(req.getParameter("offset"));
        calc.setHubLength(req.getParameter("hubLength"));
        calc.setLeftFlangeDiameter(req.getParameter("leftFlangeDiameter"));
        calc.setRightFlangeDiameter(req.getParameter("rightFlangeDiameter"));
        calc.setLeftFlangeDistance(req.getParameter("leftFlangeDistance"));
        calc.setRightFlangeDistance(req.getParameter("rightFlangeDistance"));
        calc.setSpokeHole(req.getParameter("spokeHole"));
        calc.setNumberSpokes(req.getParameter("numberSpokes"));
        calc.setLeftCrossing(req.getParameter("leftCrossing"));
        calc.setRightCrossing(req.getParameter("rightCrossing"));

        Wheelset whs = new Wheelset();
        whs.setName(wheelsetName);
        whs.setKey(calc.getKey());

        Calc c = calc.getCalcObject();

        Wheel wh = new Wheel();
        wh.setKey(c.getKey());
        wh.setEffectiveRimDiameter(c.getEffectiveRimDiameter());
        wh.setRimOffset(c.getRimOffset());
        wh.setHubLength(c.getHubLength());
        wh.setLeftFlangeDiameter(c.getLeftFlangeDiameter());
        wh.setRightFlangeDiameter(c.getRightFlangeDiameter());
        wh.setLeftFlangeDistance(c.getLeftFlangeDistance());
        wh.setRightFlangeDistance(c.getRightFlangeDistance());
        wh.setSpokeHole(c.getSpokeHole());
        wh.setLeftIntersections((long) c.getLeftNumCrossing());
        wh.setRightIntersections((long) c.getRightNumCrossing());
        wh.setSpokeNum((long) c.getNumSpokes());
        wh.setLeftLength(c.getLeftLen());
        wh.setRightLength(c.getRightLen());

        whs.setWheels(Arrays.asList(wh));

        Key wheelsetKey = DatabaseWorker.storeWheelset(whs, user);

        Map<String, Object> result = new HashMap<String, Object>();
        if (wheelsetKey == null) {
            result.put("_ERROR_MESSAGE_", "Something went wrong, we was not able to store the wheelset.");
        } else {
            result.put("wheelsetId", KeyFactory.keyToString(wheelsetKey));
        }
        Gson gson = new Gson();
        CommonUtils.writeJSONtoResponse(gson.toJson(result), resp);
    }

}
