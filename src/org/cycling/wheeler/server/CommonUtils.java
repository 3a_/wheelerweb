/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
package org.cycling.wheeler.server;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Locale;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;


public class CommonUtils {
    private static final Logger log = Logger.getLogger(CommonUtils.class.getName());

    public static void writeJSONtoResponse(String json, HttpServletResponse response) {
        if (GenericValidator.isBlankOrNull(json)) {
            log.severe("JSON string is empty; fatal error!");
            return;
        }

        // set the X-JSON content type
        response.setContentType("application/json; charset=utf-8");
        // jsonStr.length is not reliable for unicode characters
        try {
            response.setContentLength(json.getBytes("UTF8").length);
        } catch (UnsupportedEncodingException e) {
            log.severe("Problems with Json encoding: " + e.getMessage());
        }

        // return the JSON String
        Writer out;
        try {
            out = response.getWriter();
            out.write(json);
            out.flush();
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }

    /**
     * Return the most correct locale for the request.
     */
    public static Locale getLocale(HttpServletRequest req) {
        Locale locale = null;
        HttpSession session = req.getSession();
        if (session != null) {
            locale = (Locale) session.getAttribute("locale");
        }
        if (locale == null) {
            locale = req.getLocale();
        }
        if (locale == null) {
            locale = Locale.getDefault();
        }
        return locale;
    }
}
