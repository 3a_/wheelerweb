/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.model;

import java.util.Map;

import org.cycling.wheeler.api.Constants;

public class Wheel {
    private String key;

    /** Name of the wheel */
    private String name;

    /** Name or other arbitrary data about the rim */
    private String rim;
    private Double effectiveRimDiameter;
    private Double rimOffset;

    /** Name or other arbitrary info about the hub */
    private String hub;
    private Double hubLength;
    private Double leftFlangeDiameter;
    private Double rightFlangeDiameter;
    private Double leftFlangeDistance;
    private Double rightFlangeDistance;
    private Double spokeHole;

    /** Name and other arbitrary data about spokes */
    private String spokes;
    private Long spokeNum;
    private Long leftIntersections;
    private Long rightIntersections;
    private Double leftLength;
    private Double rightLength;

    public Wheel() {}

    public Wheel(String key, Map<String, Object> properties) {
        this.key = key;
        if (properties != null && properties.size() > 0) {
            name = (String) properties.get(Constants.Wheel.P_NAME);

            rim = (String) properties.get(Constants.Wheel.P_RIM);
            effectiveRimDiameter = (Double) properties.get(Constants.Wheel.P_ERD);
            rimOffset = (Double) properties.get(Constants.Wheel.P_OFFSET);

            hub = (String) properties.get(Constants.Wheel.P_HUB);
            hubLength = (Double) properties.get(Constants.Wheel.P_HUB_LENGTH);
            leftFlangeDiameter = (Double) properties.get(Constants.Wheel.P_L_FLANGE_DIAM);
            rightFlangeDiameter = (Double) properties.get(Constants.Wheel.P_R_FLANGE_DIAM);
            leftFlangeDistance = (Double) properties.get(Constants.Wheel.P_L_FLANGE_DIST);
            rightFlangeDistance = (Double) properties.get(Constants.Wheel.P_R_FLANGE_DIST);
            spokeHole = (Double) properties.get(Constants.Wheel.P_SPOKE_HOLE);

            spokes = (String) properties.get(Constants.Wheel.P_SPOKES);
            spokeNum = (Long) properties.get(Constants.Wheel.P_SPOKE_NUM);
            leftIntersections = (Long) properties.get(Constants.Wheel.P_L_INTERSECTIONS);
            rightIntersections = (Long) properties.get(Constants.Wheel.P_R_INTERSECTIONS);
            leftLength = (Double) properties.get(Constants.Wheel.P_L_LENGTH);
            rightLength = (Double) properties.get(Constants.Wheel.P_R_LENGTH);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRim() {
        return rim;
    }

    public void setRim(String rim) {
        this.rim = rim;
    }

    public Double getEffectiveRimDiameter() {
        return effectiveRimDiameter;
    }

    public void setEffectiveRimDiameter(Double effectiveRimDiameter) {
        this.effectiveRimDiameter = effectiveRimDiameter;
    }

    public Double getRimOffset() {
        return rimOffset;
    }

    public void setRimOffset(Double rimOffset) {
        this.rimOffset = rimOffset;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public Double getHubLength() {
        return hubLength;
    }

    public void setHubLength(Double hubLength) {
        this.hubLength = hubLength;
    }

    public Double getLeftFlangeDiameter() {
        return leftFlangeDiameter;
    }

    public void setLeftFlangeDiameter(Double leftFlangeDiameter) {
        this.leftFlangeDiameter = leftFlangeDiameter;
    }

    public Double getRightFlangeDiameter() {
        return rightFlangeDiameter;
    }

    public void setRightFlangeDiameter(Double rightFlangeDiameter) {
        this.rightFlangeDiameter = rightFlangeDiameter;
    }

    public Double getLeftFlangeDistance() {
        return leftFlangeDistance;
    }

    public void setLeftFlangeDistance(Double leftFlangeDistance) {
        this.leftFlangeDistance = leftFlangeDistance;
    }

    public Double getRightFlangeDistance() {
        return rightFlangeDistance;
    }

    public void setRightFlangeDistance(Double rightFlangeDistance) {
        this.rightFlangeDistance = rightFlangeDistance;
    }

    public Double getSpokeHole() {
        return spokeHole;
    }

    public void setSpokeHole(Double spokeHole) {
        this.spokeHole = spokeHole;
    }

    public String getSpokes() {
        return spokes;
    }

    public void setSpokes(String spokes) {
        this.spokes = spokes;
    }

    public Long getSpokeNum() {
        return spokeNum;
    }

    public void setSpokeNum(Long spokeNum) {
        this.spokeNum = spokeNum;
    }

    public Long getLeftIntersections() {
        return leftIntersections;
    }

    public void setLeftIntersections(Long leftIntersections) {
        this.leftIntersections = leftIntersections;
    }

    public Long getRightIntersections() {
        return rightIntersections;
    }

    public void setRightIntersections(Long rightIntersections) {
        this.rightIntersections = rightIntersections;
    }

    public Double getLeftLength() {
        return leftLength;
    }

    public void setLeftLength(Double leftLength) {
        this.leftLength = leftLength;
    }

    public Double getRightLength() {
        return rightLength;
    }

    public void setRightLength(Double rightLength) {
        this.rightLength = rightLength;
    }
}
