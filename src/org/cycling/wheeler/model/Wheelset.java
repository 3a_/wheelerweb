/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cycling.wheeler.api.Constants;

import com.google.appengine.api.datastore.Text;

public class Wheelset {
    private String key;
    private String name;
    private String description;
    private Date dateCreated;
    private Date dateChanged;
    private String gid;

    private List<Wheel> wheels;

    public Wheelset() {}

    public Wheelset(String key, Map<String, Object> properties) {
        this.key = key;
        if (properties != null && properties.size() > 0) {
            name = (String) properties.get(Constants.Wheelset.P_NAME);
            dateCreated = (Date) properties.get(Constants.Wheelset.P_CREATED);
            dateChanged = (Date) properties.get(Constants.Wheelset.P_CHANGED);
            gid = (String) properties.get(Constants.Wheelset.P_GID);
            if (properties.containsKey(Constants.Wheelset.P_DESCR)) {
                description = ((Text) properties.get(Constants.Wheelset.P_DESCR)).getValue();
            }
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(Date dateChanged) {
        this.dateChanged = dateChanged;
    }

    public String getGid() {
    	return gid;
    }

    public void setGid(String gid) {
    	this.gid = gid;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

}
