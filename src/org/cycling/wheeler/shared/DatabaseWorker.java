/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.cycling.wheeler.api.Constants;
import org.cycling.wheeler.model.Wheel;
import org.cycling.wheeler.model.Wheelset;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.users.User;

public class DatabaseWorker {
    private static final Logger log = Logger.getLogger(DatabaseWorker.class.getName());

    public static List<Map<String, Object>> findWheelsetsAll(User user) {
        List<Map<String, Object>> wheelsets = new ArrayList<Map<String, Object>>();
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        if (user != null) {
            Key groupRoot = DatabaseWorker.updateUserRef(user, ds);

            Query userWheelsets = new Query(Constants.Wheelset.KIND).setAncestor(groupRoot);
            List<Entity> storedWheelsets = ds.prepare(userWheelsets).asList(FetchOptions.Builder.withDefaults());
            if (storedWheelsets != null && storedWheelsets.size() > 0) {
                for (Entity entity : storedWheelsets) {
                    Map<String, Object> value = new HashMap<String, Object>(entity.getProperties());
                    value.put("key", KeyFactory.keyToString(entity.getKey()));
                    wheelsets.add(value);
                    
                }
            }
        }

        return wheelsets;
    }

    public static Key storeWheelset(Wheelset whs, User user) {
        Key wheelsetKey = null;
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

        Transaction tx = ds.beginTransaction();
        try {
            Key groupRoot = updateUserRef(user, ds);

            Entity wheelset = null;

            Date now = new Date();

            String id = whs.getKey();
            if (!GenericValidator.isBlankOrNull(id)) {
                Key key = KeyFactory.stringToKey(id);
                try {
                    wheelset = ds.get(key);
                } catch (EntityNotFoundException e) {
                    wheelset = new Entity(Constants.Wheelset.KIND, groupRoot);
                }
            } else {
                wheelset = new Entity(Constants.Wheelset.KIND, groupRoot);
            }

            wheelset.setProperty(Constants.Wheelset.P_NAME, whs.getName()); // name is mandatory
            String wsDesc = whs.getDescription();
            if (wsDesc != null) {
                wheelset.setProperty(Constants.Wheelset.P_DESCR, new Text(wsDesc));
            }

            wheelset.setProperty(Constants.Wheelset.P_CREATED, whs.getDateCreated() == null ? now : whs.getDateCreated());
            wheelset.setProperty(Constants.Wheelset.P_CHANGED, whs.getDateChanged() == null ? now : whs.getDateChanged());

            wheelsetKey = ds.put(wheelset);

            List<Wheel> wheels = whs.getWheels();
            // right now we supports only wheel per wheelset
            if (wheels != null && wheels.size() > 0) {
                for (Wheel w : wheels) {
                    String wheelId = w.getKey();
                    Entity wheelEntity =
                            GenericValidator.isBlankOrNull(wheelId) ? new Entity(Constants.Wheel.KIND, wheelsetKey) : new Entity(KeyFactory.stringToKey(wheelId));

                    setWheelProps(wheelEntity, w);

                    ds.put(wheelEntity);
                }
            }
        
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }

        return wheelsetKey;
    }

    /**
     * Delete wheelset and all related entities. Current user should be owner of the wheelset.
     * 
     * @param wheelsetKey Entity key in string representation
     * @param user User object
     * @return 
     *   One of the HTTP codes.
     * @throws EntityNotFoundException
     */
    public static int deleteWheelset(String wheelsetKey, User user) {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        DatabaseWorker.updateUserRef(user, ds);

        Transaction tx = ds.beginTransaction();
        try {
            Key key = KeyFactory.stringToKey(wheelsetKey);
            Entity wheelset = ds.get(key);

            // ensure the user is owner of the wheelset to delete
            Entity userLogin = ds.get(wheelset.getParent());
            Email owner = (Email) userLogin.getProperty(Constants.UserLogin.P_USER_LOGIN_ID);
            if (user.getEmail().equalsIgnoreCase(owner.getEmail())) {
                Query q = new Query(key).setKeysOnly();
                List<Entity> childWheels = ds.prepare(q).asList(FetchOptions.Builder.withDefaults());
                if (childWheels != null && childWheels.size() > 0) {
                    List<Key> keys = new ArrayList<Key>(childWheels.size());
                    for (Entity e : childWheels) {
                        keys.add(e.getKey());
                    }
                    ds.delete(keys);
                }
            } else {
                log.severe("Only owner can delete the wheelset");
                return HttpServletResponse.SC_UNAUTHORIZED;
            }

            ds.delete(key);
            tx.commit();
        } catch (EntityNotFoundException e1) {
            log.severe(e1.getMessage());
            return HttpServletResponse.SC_NOT_FOUND;
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }

        return HttpServletResponse.SC_OK;
    }

    public static Wheelset getWheelset(String wheelsetKey, Boolean itemsOnly, User user) {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        DatabaseWorker.updateUserRef(user, ds);

        try {
            Key key = KeyFactory.stringToKey(wheelsetKey);
            Entity wheelset = ds.get(key);

            // ensure the user is owner of the wheelset to delete
            Entity userLogin = ds.get(wheelset.getParent());
            Email owner = (Email) userLogin.getProperty(Constants.UserLogin.P_USER_LOGIN_ID);
            if (user.getEmail().equalsIgnoreCase(owner.getEmail())) {
                Wheelset result = new Wheelset(wheelsetKey, itemsOnly == null || !itemsOnly ? wheelset.getProperties() : null);
                List<Entity> wheels = ds.prepare(new Query(key)).asList(FetchOptions.Builder.withDefaults());
                if (wheels != null && wheels.size() > 0) {
                    List<Wheel> items = new ArrayList<Wheel>(wheels.size());
                    for (Entity w : wheels) {
                        if (Constants.Wheel.KIND.equals(w.getKind())) {
                            Wheel wheel = new Wheel(KeyFactory.keyToString(w.getKey()), w.getProperties());
                            items.add(wheel);
                        }
                    }
                    result.setWheels(items);
                }

                return result;
            }

        } catch (EntityNotFoundException e) {
            log.severe(e.getMessage());
            return null;
        }

        return null;
    }

    /**
     * Convenience method that helps to convert Wheel model object into the entity.
     */
    public static void setWheelProps(Entity e, Wheel w) {
        if (w.getName() != null)
            e.setProperty(Constants.Wheel.P_NAME, w.getName());
        if (w.getRim() != null)
            e.setProperty(Constants.Wheel.P_RIM, w.getRim());
        if (w.getEffectiveRimDiameter() != null)
            e.setUnindexedProperty(Constants.Wheel.P_ERD, w.getEffectiveRimDiameter());
        if (w.getRimOffset() != null)
            e.setUnindexedProperty(Constants.Wheel.P_OFFSET, w.getRimOffset());
        if (w.getHub() != null)
            e.setProperty(Constants.Wheel.P_HUB, w.getHub());
        if (w.getHubLength() != null)
            e.setUnindexedProperty(Constants.Wheel.P_HUB_LENGTH, w.getHubLength());
        if (w.getLeftFlangeDiameter() != null)
            e.setUnindexedProperty(Constants.Wheel.P_L_FLANGE_DIAM, w.getLeftFlangeDiameter());
        if (w.getRightFlangeDiameter() != null)
            e.setUnindexedProperty(Constants.Wheel.P_R_FLANGE_DIAM, w.getRightFlangeDiameter());
        if (w.getLeftFlangeDistance() != null)
            e.setUnindexedProperty(Constants.Wheel.P_L_FLANGE_DIST, w.getLeftFlangeDistance());
        if (w.getRightFlangeDistance() != null)
            e.setUnindexedProperty(Constants.Wheel.P_R_FLANGE_DIST, w.getRightFlangeDistance());
        if (w.getSpokeHole() != null)
            e.setUnindexedProperty(Constants.Wheel.P_SPOKE_HOLE, w.getSpokeHole());
        if (w.getSpokes() != null)
            e.setProperty(Constants.Wheel.P_SPOKES, w.getSpokes());
        if (w.getSpokeNum() != null)
            e.setUnindexedProperty(Constants.Wheel.P_SPOKE_NUM, w.getSpokeNum());
        if (w.getLeftIntersections() != null)
            e.setUnindexedProperty(Constants.Wheel.P_L_INTERSECTIONS, w.getLeftIntersections());
        if (w.getRightIntersections() != null)
            e.setUnindexedProperty(Constants.Wheel.P_R_INTERSECTIONS, w.getRightIntersections());
        if (w.getLeftLength() != null)
            e.setUnindexedProperty(Constants.Wheel.P_L_LENGTH, w.getLeftLength());
        if (w.getRightLength() != null)
            e.setUnindexedProperty(Constants.Wheel.P_R_LENGTH, w.getRightLength());
    }

    /**
     * Add user's record to UserLogin if this is first time login 
     * or update timestamp and usage counter.
     */
    public static Key updateUserRef(User user, DatastoreService ds) {
        Date now = new Date();
        Email email = new Email(user.getEmail());

        // try to find if it exists already
        Filter userLoginFilter = new FilterPredicate(Constants.UserLogin.P_USER_LOGIN_ID, FilterOperator.EQUAL, email);
        Query q = new Query(Constants.UserLogin.KIND).setFilter(userLoginFilter);
        PreparedQuery pq = ds.prepare(q);
        Entity userLogin = pq.asSingleEntity();
        if (userLogin == null) {
            userLogin = new Entity(Constants.UserLogin.KIND);
            userLogin.setProperty(Constants.UserLogin.P_USER_LOGIN_ID, email);
            userLogin.setProperty(Constants.UserLogin.P_USED_COUNT, Integer.valueOf(1));
        } else {
            // increment usage counter
            Long usedCount = (Long) userLogin.getProperty(Constants.UserLogin.P_USED_COUNT);
            userLogin.setProperty(Constants.UserLogin.P_USED_COUNT, (usedCount == null ? Long.valueOf(1L) : (usedCount + 1)));
        }
        userLogin.setProperty(Constants.UserLogin.P_LAST_LOGGED_IN, now);

        return ds.put(userLogin);
    }

}
