/*
  Licensed to the Apache Software Foundation (ASF) under one
  or more contributor license agreements.  See the NOTICE file
  distributed with this work for additional information
  regarding copyright ownership.  The ASF licenses this file
  to you under the Apache License, Version 2.0 (the
  "License"); you may not use this file except in compliance
  with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing,
  software distributed under the License is distributed on an
  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  KIND, either express or implied.  See the License for the
  specific language governing permissions and limitations
  under the License.
 */
package org.cycling.wheeler.shared;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Calc implements Serializable {

    //TODO: I don't like idea store datastore key in Cacl instance. Need to think how to move this to CalcBean
    // but this make sense when we start to operate with multiple wheels per wheelset.
    private String key;

    private Double effectiveRimDiameter;
    private Double rimOffset;
    private Double leftFlangeDiameter;
    private Double rightFlangeDiameter;
    private Double leftFlangeDistance;
    private Double rightFlangeDistance;
    private Double hubLength;
    private Double spokeHole;
    private Integer numSpokes;
    private Integer leftNumCrossing;
    private Integer rightNumCrossing;

    /**
     * Calculate spoke length for the left side.
     * 
     * @return Exact length of the spoke.
     */
    public Double getLeftLen() {
        if (
                isValid(effectiveRimDiameter) &&
                isValid(leftFlangeDiameter) &&
                isValid(leftFlangeDistance) &&
                isValid(spokeHole) &&
                isValid(numSpokes) &&
                isValid(hubLength) &&
                leftNumCrossing != null) {

            Double offset = isValid(rimOffset) ? rimOffset : 0.0;
            if (isValid(rightFlangeDistance) && leftFlangeDistance < rightFlangeDistance) {
                offset = offset * -1;
            }

            return calcSide(leftFlangeDiameter, leftFlangeDistance, leftNumCrossing, offset);
        }
        return null;
    }

    /**
     * Calculate spoke length for the right side.
     * 
     * @return Exact length of the spoke.
     */
    public Double getRightLen() {
        if (
                isValid(effectiveRimDiameter) &&
                isValid(rightFlangeDiameter) &&
                isValid(rightFlangeDistance) &&
                isValid(spokeHole) &&
                isValid(numSpokes) &&
                isValid(hubLength) &&
                rightNumCrossing != null) {

            Double offset = isValid(rimOffset) ? rimOffset : 0.0;
            if (isValid(leftFlangeDistance) && rightFlangeDistance < leftFlangeDistance) {
                offset = offset * -1;
            }

            return calcSide(rightFlangeDiameter, rightFlangeDistance, rightNumCrossing, offset);
        }
        return null;
    }

    /**
     * Round spoke length for the left side to the neatest integer that less than given value.
     * 
     * @param val Exact length of the spoke. May be null, in this case method internally calls getLeftLen().
     * @return Rounded length of the spoke.
     */
    public Double getLeftLenRounded(Double val) {
        if (val == null) {
            val = getLeftLen();
        }
        return val == null ? null : Math.floor(val);
    }

    /**
     * Round spoke length for the right side to the neatest integer that less than given value.
     * 
     * @param val Exact length of the spoke. May be null, in this case method internally calls getLeftLen().
     * @return Rounded length of the spoke.
     */
    public Double getRightLenRounded(Double val) {
        if (val == null) {
            val = getRightLen();
        }
        return val == null ? null : Math.floor(val);
    }

    /**
     * This method calculates spoke length for one of the side and receives with arguments
     * only values that may be different for sides.
     * 
     * @param flgDiameter
     * @param flgDistance
     * @param crossing
     * @param offset value that is added to "flange to center" size. Should be positive for disk(front)/drive(rear) side or negative otherwise.
     * @return
     */
    private Double calcSide(Double flgDiameter, Double flgDistance, Integer crossing, Double offset) {
        Double flangeToCenter = (hubLength / 2 - flgDistance) + offset;
        Double spokeAngle = Integer.valueOf(360 * crossing / (numSpokes/2)).doubleValue();
        Double flangeRadius = flgDiameter / 2;
        Double rimRadius = effectiveRimDiameter / 2;
        Double spokeLength = null;
        if (crossing.compareTo(0) == 0) {
            // radially spoked wheel
            spokeLength = Math.sqrt(Math.pow(flangeToCenter - spokeHole/2, 2.0) + Math.pow(rimRadius - flangeRadius, 2.0));
        } else {
            spokeLength = Math.sqrt(Math.pow(flangeToCenter, 2.0) + Math.pow(flangeRadius, 2.0) + Math.pow(rimRadius, 2.0) - 2 * (flangeRadius) * rimRadius * Math.cos(Math.toRadians(spokeAngle)) - spokeHole/2);
        }
        return spokeLength;
    }

    private boolean isValid(Integer v) {
        return (v != null && v.compareTo(0) > 0 ? true : false);
    }

    private boolean isValid(Double v) {
        return (v != null && v.compareTo(Double.MIN_VALUE) > 0 ? true : false);
    }

    public Double getEffectiveRimDiameter() {
        return effectiveRimDiameter;
    }
    public void setEffectiveRimDiameter(Double effectiveRimDiameter) {
        this.effectiveRimDiameter = effectiveRimDiameter;
    }
    public Double getRimOffset() {
        return rimOffset;
    }
    public void setRimOffset(Double rimOffset) {
        this.rimOffset = rimOffset;
    }
    public Double getLeftFlangeDiameter() {
        return leftFlangeDiameter;
    }
    public void setLeftFlangeDiameter(Double leftFlangeDiameter) {
        this.leftFlangeDiameter = leftFlangeDiameter;
    }
    public Double getRightFlangeDiameter() {
        return rightFlangeDiameter;
    }
    public void setRightFlangeDiameter(Double rightFlangeDiameter) {
        this.rightFlangeDiameter = rightFlangeDiameter;
    }
    public Double getLeftFlangeDistance() {
        return leftFlangeDistance;
    }
    public void setLeftFlangeDistance(Double leftFlangeDistance) {
        this.leftFlangeDistance = leftFlangeDistance;
    }
    public Double getRightFlangeDistance() {
        return rightFlangeDistance;
    }
    public void setRightFlangeDistance(Double rightFlangeDistance) {
        this.rightFlangeDistance = rightFlangeDistance;
    }
    public Double getHubLength() {
        return hubLength;
    }
    public void setHubLength(Double hubLength) {
        this.hubLength = hubLength;
    }
    public Double getSpokeHole() {
        return spokeHole;
    }
    public void setSpokeHole(Double spokeHole) {
        this.spokeHole = spokeHole;
    }
    public Integer getNumSpokes() {
        return numSpokes;
    }
    public void setNumSpokes(Integer numSpokes) {
        this.numSpokes = numSpokes;
    }
    public Integer getLeftNumCrossing() {
        return leftNumCrossing;
    }
    public void setLeftNumCrossing(Integer leftNumCrossing) {
        this.leftNumCrossing = leftNumCrossing;
    }
    public Integer getRightNumCrossing() {
        return rightNumCrossing;
    }
    public void setRightNumCrossing(Integer rightNumCrossing) {
        this.rightNumCrossing = rightNumCrossing;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
}
